// ESM syntax is supported.
import console from 'chalk-console'
import _ from 'lodash'
import NodeCache from 'node-cache'
import restify from 'restify'
import corsMiddleware from 'restify-cors-middleware'
import errors from 'restify-errors'
import validator from 'restify-joi-middleware'

import ERR_NAME from 'constants/errorsName'
import { PORT } from '~root/configSys'
import * as phuong from 'utils/rsa'
import * as aes from 'utils/aes'
import * as des from 'utils/des'
import { resError } from 'utils/res'
import mongoose from 'mongoose'
import { MONGO_OPTIONS } from './configSys'
import Md5Dao from 'dao/md5'
const Cipher = require('caesar-ciphers').defaultCipher;

// import * as dm from './configSys'
// console.log(dm, '----dm')


/* #region  MARK SERVE STATIC FILES */

const cache = new NodeCache({ stdTTL: 500, checkperiod: 30 })

const server = restify.createServer({
  name: 'restfull api',
  version: '0.0.1',
  formatters: {
    'application/json': (req, res, payload) => {
      // in formatters, body is the object passed to res.send() NOTE  read: https://github.com/restify/errors/pull/87
      if (_.isError(payload)) {
        const error = payload.body
        return JSON.stringify({
          code: _.get(error, 'code', ERR_NAME.COMMON.InternalServer),
          name: payload.name || 'Unknow',
          message: _.get(error, 'message', payload.message),
          ...payload.context,
        })
      }
      // for everything else, stringify as normal
      return JSON.stringify(payload)
    },
  },
})

// MARK  global vairables
global._ = _
global.isDev = process.env.NODE_ENV !== 'production'
global.cache = cache

/* MARK  Middleware  */
const cors = corsMiddleware({
  origins: ['http://127.0.0.1', '*'], // defaults to ['*']
  methods: ['GET', 'PUT', 'PATCH', 'DELETE', 'POST', 'OPTIONS'],
  preflightMaxAge: 5, // Optional
  allowHeaders: ['Authorization'],
  // exposeHeaders: ["API-Token-Expiry"]
})
server.pre(cors.preflight)
server.pre(restify.plugins.pre.dedupeSlashes())
server.pre(restify.plugins.pre.sanitizePath())
server.use(cors.actual)
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.jsonp())
server.use(restify.plugins.gzipResponse())
server.use(restify.plugins.bodyParser())
server.use(
  validator({
    joiOptions: {
      convert: true,
      allowUnknown: true,
      abortEarly: false,
      // .. all additional joi options
    },
    // changes the request keys validated keysToValidate: ['params', 'body', 'query', 'user', 'headers', 'trailers',
    // 'files'], changes how joi errors are transformed to be returned - no error details are returned in this case
    errorTransformer: (validationInput, joiError) => {
      const tranformError = _.map(joiError.details, (err) => {
        const path = err.path.join('.')
        const item = {}
        item.type = err.type
        item.message = `${path} ${err.message}`
        return item
      })
      return new errors.InvalidArgumentError(
        {
          name: ERR_NAME.COMMON.RouteValidation,
          info: {
            errors: tranformError,
          },
        },
        'Validate route fail'
      )
    },
  })
)

/* #endregion */

// MARK  connect
console.green(`Connecting to mongo ${MONGO_OPTIONS.uri}`)
mongoose
  .connect(MONGO_OPTIONS.uri, {
    // user: MONGO_OPTIONS.user,
    // pass: MONGO_OPTIONS.pass,
    ...MONGO_OPTIONS.db_options,
    useUnifiedTopology: true 
  })
  .catch(error => console.error(error))
mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
const db = mongoose.connection
db.once('open', () => {
  console.yellow(`connected ${MONGO_OPTIONS.uri} succsesfull`)
  server.listen(PORT, async () => {
    console.blue(`Server is listening on port ${PORT}`)

    server.get('/', (req, res) => {
      res.json({ msg: 'Core API-docker good boy 2' })
    })


    /* #region  RSA */
    server.get('/genKey', async (req, res, next) => {
      try {
        let result = await phuong.genKey()
        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })
    server.post('/encryptText', async (req, res, next) => {
      try {
        const text = req.body.text
        const key = req.body.key
        const result = await phuong.encryptText(text, key)

        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })

    server.post('/decode', async (req, res, next) => {
      try {
        const text = req.body.text
        const key = req.body.key
        const result = await phuong.decryptText(text, key)

        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })

    server.post('/encryptTextDes', async (req, res, next) => {
      try {
        const text = req.body.text
        const key = req.body.key
        const result = await des.encryptText(text, key)

        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })

    server.post('/decodeDes', async (req, res, next) => {
      try {
        const text = req.body.text
        const key = req.body.key
        const result = await des.decryptText(text, key)

        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })
    /* #endregion */

    server.post('/genKeyAes', async (req, res, next) => {
      try {
        const size = req.body.size;
        let result = await aes.genKey(size)
        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })
    server.get('/genKeyDes', async (req, res, next) => {
      try {
        let result = await des.genKey()
        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })
    server.post('/encryptTextAes', async (req, res, next) => {
      try {
        const { text, key } = req.body
        const result = await aes.encode(text, key)
        res.send({ data: result })
        next()

      } catch (error) {
        resError(res, error.message)
      }
    })
    server.post('/decodeAes', async (req, res, next) => {
      try {
        const { text, key } = req.body
        const result = await aes.decode(text, key)
        res.send({ data: result })
        next()
      } catch (error) {
        resError(res, error.message)
      }
    })


    /* #region  MD5 */

    server.post('/save-md5', async (req, res, next) => {
      try {
        // const { text, hased } = req.body
        console.log(Md5Dao, '-------Md5Dao')
        const result = await Md5Dao.create(req.body)
        return res.send({ data: result })
      } catch (error) {
        resError(res, error.message)
      }
    })

    server.post('/decode-md5', async (req, res, next) => {
      try {
        const { hasedString } = req.body
        const result = await Md5Dao.findOne({ hasedString })
        console.log(result, '-----result')
        return res.send(result)
      } catch (error) {
        resError(res, error.message)
      }
    })

    /* #endregion */

    /* #region  co dien */
    server.post('/encode-codien', async (req, res, next) => {
      try {
        const { shift, value } = req.body
        var cipher = new Cipher(shift);
        const encoded = cipher.encrypt(value);
        console.log(encoded, '-----encoded')

        return res.send(encoded)
      } catch (error) {
        resError(res, error.message)
      }
    })

    server.post('/decode-codien', async (req, res, next) => {
      try {
        const { shift, value } = req.body
        var cipher = new Cipher(shift);

        const decode = cipher.decrypt(value)
        return res.send(decode)
      } catch (error) {
        resError(res, error.message)
      }
    })
    /* #endregion */
  })
})
