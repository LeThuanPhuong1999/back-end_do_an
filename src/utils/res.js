export function resSuccess (res, object = {}, success = true) {
  res.json(Object.assign(object, { success }))
}
export function resError (res, messageError) {
  res.json({ error: true, message: messageError })
}
