import AUTH from './auth'
import USER from './user'
import COMMON from './common'

export default {
  AUTH,
  USER,
  COMMON
}