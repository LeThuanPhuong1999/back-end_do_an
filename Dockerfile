FROM node:alpine

WORKDIR /usr/src/app

# install package
COPY ["package.json","./"]
RUN npm install

COPY . .
RUN npm run build:prod
RUN rm -rf src

EXPOSE 5001
CMD [ "npm", "run","start:prod" ]