// const pluginBundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const WebpackBar = require('webpackbar')

module.exports = {
  watch: true,
  plugins: [
    // new pluginBundleAnalyzerPlugin(),
    new WebpackBar({
      name: 'Server',
      color: 'green', // string | hex
      reporter: {
        // lifecycle function, xem chi tiet trong docs
      },
    }),
  ],
}
