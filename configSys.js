if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

export const PORT = 5001
export const JWT_SECRET = 'JWT_SECRET'

// prettier-ignore
export const MONGO_OPTIONS = {
  uri: `mongodb://${process.env.MONGODB_HOST}/${process.env.MONGODB_DATABASE}`,
  user: process.env.MONGODB_USER,
  pass: process.env.MONGODB_PASS,
  db_options: {
    useUnifiedTopology: false,
    useNewUrlParser: true,
    autoReconnect: true,
    reconnectInterval: 5000,
    reconnectTries: Number.MAX_VALUE
  }
}
export const AMQP = process.env.HOST_AMQP

export const FTP_API = process.env.HOST_FTP
export const AUTH_API = process.env.HOST_AUTH
export const CATEGORY_API = process.env.HOST_CATEGORY

export const LOCATION_API = process.env.CATEGORY_LOCATION_API_URL
export const LOCATION_API_KEY = process.env.CATEGORY_LOCATION_API_KEY
export const STATION_AUTO_API = process.env.STATION_AUTO_API
export const PUBLIC_URL = process.env.HOST_PUBLIC
export const AQI_API = process.env.HOST_AQI
