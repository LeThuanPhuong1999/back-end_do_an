import mongoose from 'mongoose'

var rsaSchema = new mongoose.Schema({
    plainText:{
        type:String
    },
    publicKey:{
        type:String
    },
    privateKey:{
        type:String
    },
    cipherText:{
        type:String
    }
})

const rsaModel = mongoose.model('rsa',rsaSchema);

export default rsaModel